package com.likims.likims;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailsActivity extends AppCompatActivity {

    public static final String PARAM_TITLE = "title";
    public static final String PARAM_INGREDIENTS = "ingredients";
    public static final String PARAM_HREF = "href";
    public static final String PARAM_THUMBNAIL = "thumbnail";

    public TextView mTitle;
    public TextView mIngredients;
    public TextView mHref;
    public ImageView mThumbnail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTitle = (TextView)findViewById(R.id.details_title);
        mIngredients = (TextView)findViewById(R.id.details_ingredients);
        mHref = (TextView)findViewById(R.id.details_href);
        mThumbnail = (ImageView)findViewById(R.id.details_image);

        Intent intent = getIntent();
        if (intent != null) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                mTitle.setText(Html.fromHtml(extras.getString(PARAM_TITLE)));
                mIngredients.setText(extras.getString(PARAM_INGREDIENTS));
                mHref.setText(extras.getString(PARAM_HREF));

                String thumbnail = extras.getString(PARAM_THUMBNAIL);
                if (!TextUtils.isEmpty(thumbnail)) {
                    Picasso.with(getApplicationContext())
                            .load(extras.getString(PARAM_THUMBNAIL))
                            .into(mThumbnail);
                }
                else {
                    Picasso.with(getApplicationContext())
                            .load(R.drawable.default_recipe)
                            .into(mThumbnail);
                }
            }
        }
    }

}
