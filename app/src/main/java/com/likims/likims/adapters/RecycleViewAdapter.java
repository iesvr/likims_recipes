package com.likims.likims.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.likims.likims.R;
import com.likims.likims.RecipesFragment;
import com.likims.likims.datamodels.Recipe;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.CardItemViewHolder> {

    public static final String TAG = "RecycleAdapterView";

    public interface OnAdapterClickListener {
        public void onAdapterItemSelected(int position);
    }

    public ArrayList<Recipe> mRecipes;
    public Context mContext;

    public OnAdapterClickListener mCallback;

    public static class CardItemViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        ImageView image;
        TextView title;
        TextView ingredients;
        TextView link;

        CardItemViewHolder(View itemView) {
            super(itemView);

            cardView = (CardView)itemView.findViewById(R.id.card_view);
            image = (ImageView)itemView.findViewById(R.id.card_image);
            title = (TextView)itemView.findViewById(R.id.card_title);
            ingredients = (TextView)itemView.findViewById(R.id.card_ingredients);
            link = (TextView)itemView.findViewById(R.id.card_link);
        }
    }

    public RecycleViewAdapter(ArrayList<Recipe> recipes) {
        mRecipes = recipes;
        mCallback = null;
    }

    @Override
    public int getItemCount() {
        return mRecipes.size();
    }

    @Override
    public CardItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_recipes_item, viewGroup, false);
        CardItemViewHolder holder = new CardItemViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CardItemViewHolder holder, int i) {
        Recipe recipe = mRecipes.get(i);

        holder.title.setText(Html.fromHtml(recipe.title));
        holder.ingredients.setText(recipe.ingredients);
        holder.link.setText(recipe.href);

        if ((recipe.thumbnail != null) && (recipe.thumbnail.length() > 0)) {
            Picasso.with(mContext)
                    .load(recipe.thumbnail)
                    .into(holder.image);
        } else {
            Picasso.with(mContext)
                    .load(R.drawable.default_recipe)
                    .into(holder.image);
        }

        final int position = i;
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallback != null)
                    mCallback.onAdapterItemSelected(position);
            }
        });
    }

    public void setCallback(OnAdapterClickListener callback) {
        mCallback = callback;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}
