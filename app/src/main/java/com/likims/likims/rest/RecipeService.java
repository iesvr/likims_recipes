package com.likims.likims.rest;

import com.likims.likims.datamodels.RecipesList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RecipeService {

    @GET("?p=1")
    Call<RecipesList> getRecipes(@Query("q") String query);

}
