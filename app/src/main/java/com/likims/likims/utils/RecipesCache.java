package com.likims.likims.utils;

import com.likims.likims.datamodels.Recipe;

import java.util.ArrayList;

public class RecipesCache {

    protected ArrayList<Recipe> mRecipes;

    public RecipesCache() {
        init();
    }

    private void init() {
        mRecipes = new ArrayList<>();
    }

    public void setRecipes(ArrayList<Recipe> recipes) {
        mRecipes.clear();

        if (recipes != null)
            mRecipes.addAll(recipes);
    }

    public ArrayList<Recipe> getRecipes() {
        return mRecipes;
    }

    public ArrayList<Recipe> getFilteredRecipes(String query) {
        ArrayList<Recipe> result = new ArrayList<>();

        if ((mRecipes == null) || (mRecipes.size() == 0))
            return result;








        //TODO
        return mRecipes;
    }

}
