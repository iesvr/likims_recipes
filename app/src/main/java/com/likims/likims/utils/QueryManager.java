package com.likims.likims.utils;

import android.text.TextUtils;

public class QueryManager {

    protected String mQuery;
    protected String mLastQuery;

    public QueryManager() {
    }

    private void init() {
        mQuery = "";
        mLastQuery = "";
    }

    public void applyQuery(String query) {
        applyQuery(query, true);
    }

    public void applyQuery(String query, boolean lowerCase) {
        mLastQuery = mQuery;
        query = StringUtilities.nonNullString(query);
        mQuery = (lowerCase ? query.toLowerCase() : query);
    }

    public String getQuery() {
        return mQuery;
    }

    public void setQuery(String query) {
        mQuery = query;
    }

    public boolean isNewSearchQuery() {
        if (TextUtils.isEmpty(mQuery) || TextUtils.isEmpty(mLastQuery))
            return true;

        if (mQuery.length() > mLastQuery.length())
            return true;

        if ((mQuery.length() <= mLastQuery.length()) &&
                !mLastQuery.contains(mQuery))
            return true;

        /*
        if ((mQuery == null) ||
                (mQuery.length() == 0) ||
                ((mLastQuery != null) && (mLastQuery.length() >= 0) && (mQuery.length() > mLastQuery.length())))
            return true;

        if ((mQuery.length() <= mLastQuery.length()) &&
                !mLastQuery.contains(mQuery))
            return true;
        */

        return false;
    }

    public boolean isSubQuery() {
        if (TextUtils.isEmpty(mQuery) || TextUtils.isEmpty(mLastQuery))
            return false;

        if ((mQuery.length() <= mLastQuery.length()) &&
                mLastQuery.contains(mQuery))
            return true;

        return false;
    }

}
