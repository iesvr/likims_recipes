package com.likims.likims;

import android.annotation.TargetApi;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;

import com.likims.likims.datamodels.Recipe;
import com.likims.likims.datamodels.RecipesList;
import com.likims.likims.rest.RecipeApiClient;
import com.likims.likims.rest.RecipeService;
import com.likims.likims.utils.QueryManager;
import com.likims.likims.utils.RecipesCache;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecipesActivity extends AppCompatActivity
    implements RecipesFragment.OnRecipeSelectionListener {

    public static final String TAG = "Recipes";

    public static final String PARAM_SEARCH_QUERY = "search_query";

    public RecipeService mRecipeService;

    private Menu mMenu;

    private QueryManager mQueryManager;
    private RecipesCache mCache;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes);

        setUpData();

        setUpToolbar();
        setUpActionBar();

        setUpRestServices();
    }

    private void setUpData() {
        mQueryManager = new QueryManager();
        mCache = new RecipesCache();
    }

    private void setUpToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void setUpActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setLogo(R.drawable.ic_recipes);
        actionBar.setDisplayUseLogoEnabled(true);
    }

    private void setUpRestServices() {
        mRecipeService = RecipeApiClient.getClient().create(RecipeService.class);
    }

    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        this.mMenu = menu;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

            MenuItem searchItem = menu.findItem(R.id.action_search);
            final SearchView searchView = (SearchView) searchItem.getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setIconifiedByDefault(true);

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    //closeSearchView();
                    searchRecipesForQuery(query);
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    searchRecipesForQuery(newText);
                    return true;
                }
            });

            if (mQueryManager.getQuery() != null) {
                searchItem.expandActionView();
                searchView.setQuery(mQueryManager.getQuery(), false);
                searchView.clearFocus();
            }
        }

        return super.onCreateOptionsMenu(menu);
    }

    private void closeSearchView() {
        SearchView searchView = (SearchView) mMenu.findItem(R.id.action_search).getActionView();
        searchView.setIconified(true);
        searchView.onActionViewCollapsed();
    }

    private void searchRecipesForQuery(String query) {
        mQueryManager.applyQuery(query);

        loadRecipesViaRest(query);

        /*
        if (mQueryManager.isNewSearchQuery()) {
            loadRecipesViaRest(query);
        }
        else if (mQueryManager.isSubQuery()) {
            applyRecipesToFragment(mCache.getFilteredRecipes(query));
        }
        */
    }

    private void loadRecipesViaRest(String query) {
        mRecipeService.getRecipes(query).enqueue(new Callback<RecipesList>() {
            @Override
            public void onResponse(Call<RecipesList> call, Response<RecipesList> response) {
                if (response.isSuccessful()) {
                    ArrayList<Recipe> recipes = response.body().results;
                    mCache.setRecipes(recipes);

                    applyRecipesToFragment(mCache.getRecipes());
                }
                else {
                    Log.e(TAG, "REST ERROR (onResponse, isSuccessful == false)");
                    try {
                        Log.e(TAG, response.errorBody().string());
                    }
                    catch (IOException e) {}
                }
            }

            @Override
            public void onFailure(Call<RecipesList> call, Throwable t) {
                Log.e(TAG, "REST ERROR (onFailure)");
                Log.e(TAG, t.getMessage());
            }
        });
    }

    private void applyRecipesToFragment(ArrayList<Recipe> recipes) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.recipes_list_fragment,
                        RecipesFragment.newInstance(getApplicationContext(), recipes, mQueryManager.getQuery()))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onRecipeSelected(int position) {
        Recipe recipe = mCache.getRecipes().get(position);

        Intent intent = new Intent(getApplicationContext(), DetailsActivity.class);

        Bundle params = new Bundle();
        params.putString(DetailsActivity.PARAM_TITLE, recipe.title);
        params.putString(DetailsActivity.PARAM_INGREDIENTS, recipe.ingredients);
        params.putString(DetailsActivity.PARAM_HREF, recipe.href);
        params.putString(DetailsActivity.PARAM_THUMBNAIL, recipe.thumbnail);
        intent.putExtras(params);

        startActivity(intent);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(PARAM_SEARCH_QUERY, mQueryManager.getQuery());

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        String query = savedInstanceState.getString(PARAM_SEARCH_QUERY);
        mQueryManager.setQuery(query);
    }

}
