package com.likims.likims.datamodels;

public class Recipe {

    public String title;
    public String href;
    public String ingredients;
    public String thumbnail;

    public Recipe() {
        this(null, null, null, null);
    }

    public Recipe(String title, String href, String ingredients, String thumbnail) {
        this.title = title;
        this.href = href;
        this.ingredients = ingredients;
        this.thumbnail = thumbnail;
    }

}
