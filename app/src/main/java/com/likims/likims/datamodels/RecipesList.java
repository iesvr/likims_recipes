package com.likims.likims.datamodels;

import java.util.ArrayList;

public class RecipesList {

    public String title;
    public String version;
    public String href;
    public ArrayList<Recipe> results;

    public RecipesList() {
        this(null, null, null, new ArrayList<Recipe>());
    }

    public RecipesList(String title, String version, String href, ArrayList<Recipe> results) {
        this.title = title;
        this.version = version;
        this.href = href;
        this.results = results;
    }

}
