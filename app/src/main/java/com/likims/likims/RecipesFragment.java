package com.likims.likims;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.likims.likims.adapters.RecycleViewAdapter;
import com.likims.likims.datamodels.Recipe;

import java.util.ArrayList;

public class RecipesFragment extends Fragment implements RecycleViewAdapter.OnAdapterClickListener {

    public static final String ARG_QUERY = "query";

    public interface OnRecipeSelectionListener {
        public void onRecipeSelected(int position);
    }
    public OnRecipeSelectionListener mCallback;

    protected RecyclerView mRecyclerView;
    protected LinearLayoutManager mLayoutManager;

    public Context mContext;

    public String mQuery;

    protected ArrayList<Recipe> mRecipes;

    public RecipesFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mCallback = (OnRecipeSelectionListener)activity;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnRecipeSelectionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipes, container, false);

        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        if (mContext != null) {
            mLayoutManager = new LinearLayoutManager(mContext);
            mRecyclerView.setLayoutManager(mLayoutManager);

            RecycleViewAdapter adapter = new RecycleViewAdapter(mRecipes);
            adapter.setCallback(this);
            adapter.mContext = mContext;
            mRecyclerView.setAdapter(adapter);
        }

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        if (getArguments() != null) {
            mQuery = getArguments().getString(ARG_QUERY);
        }
    }

    public static RecipesFragment newInstance(Context context, ArrayList<Recipe> items, String query) {
        RecipesFragment fragment = new RecipesFragment();

        Bundle args = new Bundle();
        args.putString(ARG_QUERY, query);
        fragment.setArguments(args);

        fragment.mContext = context;
        fragment.mRecipes = items;

        return fragment;
    }

    @Override
    public void onAdapterItemSelected(int position) {
        mCallback.onRecipeSelected(position);
    }

}
